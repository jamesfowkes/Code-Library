#ifndef _CHECK_AND_CLEAR_H_
#define _CHECK_AND_CLEAR_H_

#ifdef __cplusplus
bool check_and_clear(bool& flag);
#endif

bool check_and_clear(bool* flag);

#endif
