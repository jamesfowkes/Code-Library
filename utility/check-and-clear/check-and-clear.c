#include <stdbool.h>

#ifdef _AVR_ARCH_
#include <util/atomic.h>
#else
#define ATOMIC_BLOCK(x)
#define ATOMIC_RESTORESTATE
#endif

bool check_and_clear(bool* pFlag)
{
    bool value = false;
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        if (pFlag)
        {
            if (*pFlag)
            {
                value = true;
                *pFlag = false;
            }
        }
    }
    return value;
}
