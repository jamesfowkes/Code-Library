/*
 * Standard Library Includes
 */

#include <stdint.h>
#include <stdbool.h>

/*
 * Generic Library Includes
 */

#include "seven_segment_map.h"

/*
 * Private Function Prototypes
 */
static uint8_t A(uint8_t digit, uint8_t bit);
static uint8_t B(uint8_t digit, uint8_t bit);
static uint8_t C(uint8_t digit, uint8_t bit);
static uint8_t D(uint8_t digit, uint8_t bit);
static uint8_t E(uint8_t digit, uint8_t bit);
static uint8_t F(uint8_t digit, uint8_t bit);
static uint8_t G(uint8_t digit, uint8_t bit);

uint8_t SSEG_CreateDigit(uint8_t digit, SEVEN_SEGMENT_MAP const * const map, bool tails)
{
	uint8_t display = 0;

	if (digit < 16)
	{
		display |= A(digit, map->A);
		display |= B(digit, map->B);
		display |= C(digit, map->C);
		display |= D(digit, map->D);
		display |= E(digit, map->E);
		display |= F(digit, map->F);
		display |= G(digit, map->G);

		if (tails)
		{
			if (digit == 6) { display |= (1 << map->A); }
			if (digit == 9) { display |= (1 << map->D); }
		}
	}

	return display;
}

void SSEG_AddDecimal(uint8_t *pDisplay, SEVEN_SEGMENT_MAP const * const map, bool add)
{
	if (add)
	{
		*pDisplay |= (1 << map->DP);
	}
	else
	{
		*pDisplay &= ~(1 << map->DP);
	}
}

static uint8_t A(uint8_t digit, uint8_t bit)
{
    static const uint16_t segmentOn = 0b1101011111101101;
	return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t B(uint8_t digit, uint8_t bit)
{
    static const uint16_t segmentOn = 0b0010011110011111;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t C(uint8_t digit, uint8_t bit)
{
    static const uint16_t segmentOn = 0b0010111111111011;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t D(uint8_t digit, uint8_t bit)
{
    static const uint16_t segmentOn = 0b0111101101101101;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t E(uint8_t digit, uint8_t bit)
{
    static const uint16_t segmentOn = 0b1111110101000101;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t F(uint8_t digit,uint8_t bit)
{
    static const uint16_t segmentOn = 0b1101111101110001;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}

static uint8_t G(uint8_t digit,uint8_t bit)
{
    static const uint16_t segmentOn = 0b1110111101111100;
    return (segmentOn & (1 << digit)) ? (1 << bit) : 0;
}
